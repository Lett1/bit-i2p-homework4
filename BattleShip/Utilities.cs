﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utilities.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines various utility functions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    using System;

    /// <summary>
    /// Provides a variety of utility functions.
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Private Random instance.
        /// </summary>
        /// <value>An instance of Random.</value>
        private static Random rnd;

        /// <summary>
        /// Gets the Random Number Generator.
        /// </summary>
        /// <value>An instance of Random.</value>
        public static Random Random
        {
            get
            {
                if (rnd == null)
                {
                    rnd = new Random();
                }

                return rnd;
            }
        }

        /// <summary>
        /// Asks the user a yes/no question. (y/n) is automatically added to the string.
        /// </summary>
        /// <returns><c>true</c>, if yes was answered, <c>false</c> otherwise.</returns>
        /// <param name="question">Main text of the question.</param>
        public static bool AskForBoolean(string question)
        {
            while (true)
            {
                Services.Graphics.Clear();
                Services.Graphics.PutString(0, 0, question + " (y/n):");

                string input = Services.Input.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                        return true;
                    case "n":
                    case "no":
                        return false;
                }
            }
        }

        /// <summary>
        /// Asks the user to press a key.
        /// </summary>
        /// <returns>The pressed key.</returns>
        /// <param name="purpose">The purpose of the key.</param>
        /// <param name="current">The current value of the key.</param>
        public static ConsoleKey AskForConsoleKey(string purpose, ConsoleKey current)
        {
            while (true)
            {
                Services.Graphics.Clear();
                Services.Graphics.PutString(0, 0, $"{purpose} is currently set to {current}");
                Services.Graphics.PutString(0, 1, $"Press a key to set a new value for {purpose} or press ESC to abort.");

                ConsoleKey input = Services.Input.ReadKey();

                if (input == ConsoleKey.Escape)
                {
                    return current;
                }
                else
                {
                    return input;
                }
            }
        }
        
        /// <summary>
        /// Allows the user to enter a number in the specified range.
        /// </summary>
        /// <returns>The for number.</returns>
        /// <param name="description">The use of the number.</param>
        /// <param name="min">Minimum of the number.</param>
        /// <param name="max">Maximum of the number.</param>
        public static int AskForNumber(string description, int min = 0, int max = 1000)
        {
            int number;

            while (true)
            {               
                Services.Graphics.Clear();
                Services.Graphics.PutString(0, 0, $"Please enter a new value for {description}.");
                Services.Graphics.PutString(0, 1, $"Accepted value is a number between {min} and {max}.");
                Services.Graphics.PutString(0, 2, "=>");

                string input = Services.Input.ReadLine();

                if (string.IsNullOrWhiteSpace(input))
                {
                    continue;
                }

                if (int.TryParse(input, out number))
                {
                    if (number < min || number > max)
                    {
                        continue;
                    }
                    
                    return number;
                }
            } 
        }

        /// <summary>
        /// Converts a number to its corresponding letter form, e.g. 1 -> A, B -> 2, etc.
        /// Numbers out of range get returned in their string form.
        /// Currently only works with numbers up to 26.
        /// </summary>
        /// <param name="number">The number to convert.</param>
        /// <returns>A Letter representing the number.</returns>
        public static string ConvertNumberToLetter(int number)
        {
            const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (number < 0 || number > Alphabet.Length)
            {
                return number.ToString();
            }

            return Alphabet[number - 1].ToString();
        }
    }
}