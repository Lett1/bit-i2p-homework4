﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmartAI.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Implements the SmartAI type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using System.Collections.Generic;

    /// <summary>
    /// Smart Battleship AI, once a hit is scored, surrounding points become prefered targets.
    /// </summary>
    public class SmartAI : IComputerPlayer
    {
        /// <summary>
        /// The potential targets.
        /// </summary>
        private List<Vector2> potentialTargets;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BattleShip.SmartAI"/> class.
        /// </summary>
        public SmartAI()
        {
            this.potentialTargets = new List<Vector2>();
        }
        
        /// <summary>
        /// Performs a turn of the game. If a hit is scored, another may be fired.
        /// </summary>
        /// <param name="playerBoard">The players board.</param>
        /// <param name="computerBoard">The computers board.</param>
        public void DoTurn(Board playerBoard, Board computerBoard)
        {
            bool turnOver = false;

            while (!turnOver)
            {
                if (playerBoard.AllShipsSunk())
                {
                    return;
                }

                Vector2 target;

                if (this.potentialTargets.Count > 0)
                {
                    int index = Utilities.Random.Next(0, this.potentialTargets.Count);
                    target = this.potentialTargets[index];
                    this.potentialTargets.RemoveAt(index);
                }
                else
                {
                    target = new Vector2(
                        Utilities.Random.Next(0, playerBoard.Grid.GetLength(0)),
                        Utilities.Random.Next(0, playerBoard.Grid.GetLength(1)));
                }

                if (playerBoard.Grid[target.X, target.Y] == TileType.Hit)
                {
                    continue;
                }

                // If we hit a ship, add the surrounding points to the list of potential targets.
                if (playerBoard.FireShot(target))
                {
                    if (target.Y - 1 > 0)
                    {
                        this.potentialTargets.Add(target.Add(new Vector2(0, -1)));
                    }

                    if (target.Y + 1 < playerBoard.Height)
                    {
                        this.potentialTargets.Add(target.Add(new Vector2(0, 1)));
                    }

                    if (target.X - 1 > 0)
                    {
                        this.potentialTargets.Add(target.Add(new Vector2(-1, 0)));
                    }

                    if (target.X + 1 < playerBoard.Width)
                    {
                        this.potentialTargets.Add(target.Add(new Vector2(1, 0)));
                    }
                }
                else
                {
                    turnOver = true;
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.SmartAI"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.SmartAI"/>.</returns>
        public override string ToString()
        {
            return "Smart";
        }
    }
}