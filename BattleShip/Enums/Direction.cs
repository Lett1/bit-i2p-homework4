﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Direction.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Direction enum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    /// <summary>
    /// Represents the four cardinal directions.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// This one points northwards.
        /// </summary>
        Up,

        /// <summary>
        /// This one points to the right.
        /// </summary>
        Right,

        /// <summary>
        /// This one goes down.
        /// </summary>
        Down,

        /// <summary>
        /// This one goes left.
        /// </summary>
        Left
    }
}