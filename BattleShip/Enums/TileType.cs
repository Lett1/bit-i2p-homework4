﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TileType.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Implements the TileType enum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    /// <summary>
    /// Tile type.
    /// </summary>
    public enum TileType
    {
        /// <summary>
        /// An empty tile.
        /// </summary>
        Empty,

        /// <summary>
        /// Tile that has been shot at.
        /// </summary>
        Hit,

        /// <summary>
        /// Tile with an unknown state.
        /// </summary>
        Unknown
    }
}