﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ShipType.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the ShipType enum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    /// <summary>
    /// The ship type.
    /// </summary>
    public enum ShipType
    {
        /// <summary>
        /// A destroyer. Shoots torpedoes.
        /// </summary>
        Destroyer,

        /// <summary>
        /// A Submarine. Hides underwater.
        /// </summary>
        Submarine,

        /// <summary>
        /// A Cruiser. Fast and deadly.
        /// </summary>
        Cruiser,

        /// <summary>
        /// A Battleship. Don't mess with this one.
        /// </summary>
        Battleship
    }
}