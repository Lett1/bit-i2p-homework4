﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConsoleRenderer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   The console renderer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using System;
    using System.Text;

    using BattleShip.Interfaces;

    /// <summary>
    /// Renders graphics to a console window.
    /// </summary>
    public class ConsoleRenderer : IRenderer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleRenderer"/> class.
        /// </summary>
        public ConsoleRenderer()
        {
            this.Setup();
        }

        /// <summary>
        /// Clear the console window.
        /// </summary>
        public void Clear()
        {
            Console.Clear();
        }

        /// <summary>
        /// Gets the background color of the console.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleColor"/>.
        /// </returns>
        public ConsoleColor GetBackgroundColor()
        {
            return Console.BackgroundColor;
        }

        /// <summary>
        /// Gets the foreground color of the console.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleColor"/>.
        /// </returns>
        public ConsoleColor GetForegroundColor()
        {
            return Console.ForegroundColor;
        }

        /// <summary>
        /// Get the console window height.
        /// </summary>
        /// <returns>
        /// The height of the viewport.
        /// </returns>
        public int GetViewportHeight()
        {
            return Console.WindowHeight;
        }

        /// <summary>
        /// Gets the console window width.
        /// </summary>
        /// <returns>
        /// The width of the viewport.
        /// </returns>
        public int GetViewportWidth()
        {
            return Console.WindowWidth;
        }

        /// <summary>
        /// The get the console window bounds.
        /// </summary>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        public Vector2 GetWindowBounds()
        {
            return new Vector2(Console.WindowWidth, Console.WindowHeight);
        }

        /// <inheritdoc />
        public void PutChar(int x, int y, char c)
        {
            if (x >= 0 && x < Console.WindowWidth && y >= 0 && y < Console.WindowHeight)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(c);
            }
        }

        /// <inheritdoc />
        public void PutString(int x, int y, string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                this.PutChar(x + i, y, text[i]);
            }
        }

        /// <summary>
        /// Resets the color settings to their defaults.
        /// </summary>
        public void ResetColors()
        {
            Console.ResetColor();
        }

        /// <summary>
        /// Sets the console background color.
        /// </summary>
        /// <param name="background">
        /// The background.
        /// </param>
        public void SetBackgroundColor(ConsoleColor background)
        {
            Console.BackgroundColor = background;
        }

        /// <summary>
        /// Sets the console foreground color.
        /// </summary>
        /// <param name="foreground">
        /// The foreground.
        /// </param>
        public void SetForegroundColor(ConsoleColor foreground)
        {
            Console.ForegroundColor = foreground;
        }

        /// <inheritdoc />
        /// <summary>
        /// Set the output encoding to UTF8 an hide the cursor.
        /// </summary>
        public void Setup()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.CursorVisible = false;
        }
    }
}