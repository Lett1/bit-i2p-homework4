﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Game.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Contains the main game logic.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A Game of Battleship.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Start a new game of battleship.
        /// </summary>
        /// <returns>Integer representing the winner of the game, 0 if the player lost, 1 if the player won.</returns>
        public int StartGame()
        {
            Configuration config = Configuration.Instance;

            IComputerPlayer ai = new CheatingAI();

            Board playerBoard = new Board(config.BoardWidth, config.BoardHeight, new Vector2(0, 0));
            Board enemyBoard = new Board(config.BoardWidth, config.BoardHeight, new Vector2(30, 0), TileType.Unknown);

            List<ShipType> shipsToPlace = new List<ShipType>();

            for (int i = 0; i < config.SubmarineCount; i++)
            {
                shipsToPlace.Add(ShipType.Submarine);
            }

            for (int i = 0; i < config.DestroyerCount; i++)
            {
                shipsToPlace.Add(ShipType.Destroyer);
            }

            for (int i = 0; i < config.CruiserCount; i++)
            {
                shipsToPlace.Add(ShipType.Cruiser);
            }

            for (int i = 0; i < config.BattleshipCount; i++)
            {
                shipsToPlace.Add(ShipType.Battleship);
            }

            if (Utilities.AskForBoolean("Do you want to place your ships automatically?"))
            {
                PlaceShipsAutomatically(shipsToPlace, playerBoard);
            }
            else
            {
                PlaceShips(shipsToPlace, playerBoard);
            }
            
            PlaceShipsAutomatically(shipsToPlace, enemyBoard);

            // Flip a coin to determine who goes first.
            if (Utilities.Random.Next(0, 101) > 50)
            {
                ai.DoTurn(playerBoard, enemyBoard);
            }

            // Main game loop
            while (true)
            {
                if (playerBoard.AllShipsSunk())
                {
                    return 0;
                }
                else if (enemyBoard.AllShipsSunk())
                {
                    return 1;
                }

                DoPlayerTurn(playerBoard, enemyBoard);
                ai.DoTurn(playerBoard, enemyBoard);
            }
        }

        /// <summary>
        /// Check whether a given ship can be placed on the board, e.g. does not reach outside the board or overlap any other ship.
        /// </summary>
        /// <param name="ship">
        /// The ship that gets placed.
        /// </param>
        /// <param name="board">
        /// The board to use for checking.
        /// </param>
        /// <returns>
        /// True of the ship can be placed on the board.
        /// </returns>
        private static bool CheckIfPositionIsValid(Ship ship, Board board)
        {
            foreach (var point in ship.GetOccupiedPoints())
            {
                // Check if the point lies inside the board.
                if (point.X < 0 || point.Y < 0 || point.X > board.Grid.GetLength(0) - 1
                    || point.Y > board.Grid.GetLength(0) - 1)
                {
                    return false;
                }

                // Check if we overlap any existing ship
                if (board.IsShipAtCoordinate(point))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Process a human players turn.
        /// </summary>
        /// <param name="playerBoard">
        /// The player board.
        /// </param>
        /// <param name="enemyBoard">
        /// The enemy board.
        /// </param>
        private static void DoPlayerTurn(Board playerBoard, Board enemyBoard)
        {
            int cursorX = enemyBoard.Grid.GetLength(0) / 2;
            int cursorY = enemyBoard.Grid.GetLength(1) / 2;

            bool turnComplete = false;

            while (!turnComplete)
            {
                Services.Graphics.Clear();

                DrawBoard(playerBoard, true, "P L A Y E R");
                DrawBoard(enemyBoard, false, "E N E M Y", "X", " ");

                if (enemyBoard.AllShipsSunk())
                {
                    return;
                }

                Services.Graphics.SetForegroundColor(ConsoleColor.Yellow);
                Services.Graphics.PutString((cursorX * 2) + enemyBoard.Offset.X, (cursorY * 2) + enemyBoard.Offset.Y + 2, "X");
                Services.Graphics.ResetColors();

                ConsoleKey input = Services.Input.ReadKey();

                if (input == Configuration.Instance.MoveUpKey)
                {
                    if (cursorY > 0)
                    {
                        cursorY--;
                    }
                }
                else if (input == Configuration.Instance.MoveDownKey)
                {
                    if (cursorY < enemyBoard.Grid.GetLength(1) - 1)
                    {
                        cursorY++;
                    }
                }
                else if (input == Configuration.Instance.MoveLeftKey)
                {
                    if (cursorX > 0)
                    {
                        cursorX--;
                    }
                }
                else if (input == Configuration.Instance.MoveRightKey)
                {
                    if (cursorX < enemyBoard.Grid.GetLength(0) - 1)
                    {
                        cursorX++;
                    }
                }
                else if (input == Configuration.Instance.FireKey)
                {
                    if (!enemyBoard.FireShot(new Vector2(cursorX, cursorY)))
                    {
                        turnComplete = true;
                    }
                }
                else if (input == ConsoleKey.Escape)
                {
                    if (Utilities.AskForBoolean("Run away like a coward?"))
                    {
                        foreach (Ship ship in playerBoard.Ships)
                        {
                            ship.TakeDamage(99999);
                        }

                        turnComplete = true;
                    }
                }
            }
        }

        /// <summary>
        /// Draw a board onto the screen, including ships, name and coordinates.
        /// </summary>
        /// <param name="board">The board to draw.</param>
        /// <param name="drawShips">Set to true to show all ships.</param>
        /// <param name="name">The name displayed under the board.</param>
        /// <param name="shipChar">Character used to represent a ship.</param>
        /// <param name="hitWaterChar">Character used to represent a hit tile.</param>
        private static void DrawBoard(
            Board board,
            bool drawShips,
            string name,
            string shipChar = "S",
            string hitWaterChar = "O")
        {
            // Draw the top coordinates.
            for (int i = 0; i < board.Grid.GetLength(0); i++)
            {
                Services.Graphics.PutString((i * 2) + board.Offset.X, board.Offset.Y, (i + 1).ToString());
            }

            // Draw the grid and coordinate letters on the right.
            for (int i = 0; i < board.Width; i++)
            {
                for (int j = 0; j < board.Height; j++)
                {
                    switch (board.Grid[i, j])
                    {
                        case TileType.Empty:
                            if (board.IsShipAtCoordinate(new Vector2(i, j)) && drawShips)
                            {
                                Services.Graphics.SetBackgroundColor(ConsoleColor.Green);
                                Services.Graphics.PutString(
                                    (i * 2) + board.Offset.X,
                                    (j * 2) + board.Offset.Y + 2,
                                    shipChar);
                            }
                            else
                            {
                                Services.Graphics.SetBackgroundColor(ConsoleColor.DarkBlue);
                                Services.Graphics.PutString(i * 2 + board.Offset.X, j * 2 + board.Offset.Y + 2, " ");
                            }

                            break;
                        case TileType.Hit:
                            if (board.IsShipAtCoordinate(new Vector2(i, j)))
                            {
                                Services.Graphics.SetBackgroundColor(ConsoleColor.Red);
                                Services.Graphics.PutString(
                                    (i * 2) + board.Offset.X,
                                    (j * 2) + board.Offset.Y + 2,
                                    shipChar);
                            }
                            else
                            {
                                Services.Graphics.SetBackgroundColor(ConsoleColor.DarkBlue);
                                Services.Graphics.SetForegroundColor(ConsoleColor.White);
                                Services.Graphics.PutString(
                                    (i * 2) + board.Offset.X,
                                    (j * 2) + board.Offset.Y + 2,
                                    hitWaterChar);
                            }

                            break;
                        case TileType.Unknown:
                            Services.Graphics.SetBackgroundColor(ConsoleColor.Gray);
                            Services.Graphics.PutString((i * 2) + board.Offset.X, (j * 2) + board.Offset.Y + 2, " ");
                            break;
                    }

                    Services.Graphics.ResetColors();

                    Services.Graphics.PutString(
                        board.Offset.X + (board.Width * 2) + 2,
                        (j * 2) + board.Offset.Y + 2,
                        Utilities.ConvertNumberToLetter(j + 1));
                }
            }

            int nameOffset = name.Length / 2;

            Services.Graphics.PutString(board.Offset.X + nameOffset, board.Offset.Y + (board.Height * 2) + 1, name);
        }

        /// <summary>
        /// Draw a single ship onto the board.
        /// </summary>
        /// <param name="ship">
        /// The ship to draw.
        /// </param>
        /// <param name="board">
        /// The board the ship is on.
        /// </param>
        /// <param name="color">
        /// The ships color.
        /// </param>
        private static void DrawShip(Ship ship, Board board, ConsoleColor color = ConsoleColor.White)
        {
            Services.Graphics.SetForegroundColor(color);
            foreach (var point in ship.GetOccupiedPoints())
            {
                Services.Graphics.PutString((point.X * 2) + board.Offset.X, (point.Y * 2) + board.Offset.Y + 2, "S");
            }

            Services.Graphics.ResetColors();
        }

        /// <summary>
        /// Place the enemies ships on the board.
        /// Positions and rotations are chosen randomly until a fit is found.
        /// </summary>
        /// <param name="shipList">List of ShipTypes to place.</param>
        /// <param name="board">The board to place the ships on.</param>
        private static void PlaceShipsAutomatically(List<ShipType> shipList, Board board)
        {
            List<Ship> unplacedShips = new List<Ship>();

            foreach (var type in shipList)
            {
                unplacedShips.Add(Ship.AssembleShip(type));
            }

            foreach (Ship ship in unplacedShips)
            {
                while (true)
                {
                    ship.Position = new Vector2(
                        Utilities.Random.Next(0, board.Grid.GetLength(0)),
                        Utilities.Random.Next(0, board.Grid.GetLength(1)));
                    ship.Direction = (Direction)Utilities.Random.Next(0, 4);

                    if (CheckIfPositionIsValid(ship, board))
                    {
                        board.Ships.Add(ship);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Allows the user to place a selection of ships on their board.
        /// </summary>
        /// <param name="shipList">List of ships to place.</param>
        /// <param name="board">The board.</param>
        private static void PlaceShips(List<ShipType> shipList, Board board)
        {
            Configuration config = Configuration.Instance;

            List<Ship> unplacedShips = new List<Ship>();

            foreach (var type in shipList)
            {
                unplacedShips.Add(Ship.AssembleShip(type));
            }

            int cursorX = board.Grid.GetLength(0) / 2;
            int cursorY = board.Grid.GetLength(1) / 2;
            int direction = (int)unplacedShips[0].Direction;

            while (unplacedShips.Count != 0)
            {
                Console.Clear();
                DrawBoard(board, true, "P L A Y E R");

                Services.Graphics.PutString(board.Width + board.Offset.X + 15, board.Offset.Y, "Ships left to place:");

                for (int index = 0; index < unplacedShips.Count; index++)
                {
                    Services.Graphics.PutString(
                        board.Width + board.Offset.X + 16,
                        board.Offset.Y + index + 1,
                        $"{unplacedShips[index].Type} {unplacedShips[index].HullNumber}");
                }

                unplacedShips[0].Position = new Vector2(cursorX, cursorY);

                if (!CheckIfPositionIsValid(unplacedShips[0], board))
                {
                    DrawShip(unplacedShips[0], board, ConsoleColor.Red);
                }
                else
                {
                    DrawShip(unplacedShips[0], board);
                }

                ConsoleKey input = Services.Input.ReadKey();

                if (input == config.MoveUpKey)
                {
                    if (cursorY > 0)
                    {
                        cursorY--;
                    }
                }
                else if (input == config.MoveDownKey)
                {
                    if (cursorY < board.Grid.GetLength(1) - 1)
                    {
                        cursorY++;
                    }
                }
                else if (input == config.MoveLeftKey)
                {
                    if (cursorX > 0)
                    {
                        cursorX--;
                    }
                }
                else if (input == config.MoveRightKey)
                {
                    if (cursorX < board.Grid.GetLength(0) - 1)
                    {
                        cursorX++;
                    }
                }
                else if (input == config.RotateShipKey)
                {
                    direction = (direction + 1) % 4;
                    unplacedShips[0].Direction = (Direction)direction;
                }
                else if (input == ConsoleKey.Enter)
                {
                    if (CheckIfPositionIsValid(unplacedShips[0], board))
                    {
                        board.Ships.Add(unplacedShips[0]);
                        unplacedShips.RemoveAt(0);
                    }
                }
            }
        }
    }
}