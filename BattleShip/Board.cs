﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Board.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the board type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a board with a grid of tiles and ships.
    /// </summary>
    public class Board
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Board"/> class.
        /// </summary>
        /// <param name="width">
        /// The width of the board.
        /// </param>
        /// <param name="height">
        /// The height of the board.
        /// </param>
        /// <param name="offset">
        /// Where to draw the board on the screen.
        /// </param>
        /// <param name="defaultTileType">
        /// The default tile type. Defaults to Empty.
        /// </param>
        public Board(int width, int height, Vector2 offset, TileType defaultTileType = TileType.Empty)
        {
            this.Grid = new TileType[width, height];

            for (int i = 0; i < this.Grid.GetLength(0); i++)
            {
                for (int j = 0; j < this.Grid.GetLength(1); j++)
                {
                    this.Grid[i, j] = defaultTileType;
                }
            }

            this.Offset = offset;
            this.Ships = new List<Ship>();
        }

        /// <summary>
        /// Gets the height of the board.
        /// </summary>
        /// <value>The height of the board.</value>
        public int Height
        {
            get
            {
                return this.Grid.GetLength(1);
            }
        }

        /// <summary>
        /// Gets the width of the board.
        /// </summary>
        /// <value>The width of the board.</value>
        public int Width
        {
            get
            {
                return this.Grid.GetLength(0);
            }
        }

        /// <summary>
        /// Gets the list of ships on this board.
        /// </summary>
        /// <value>List of ships on the board.</value>
        public List<Ship> Ships { get; private set; }

        /// <summary>
        /// Gets or sets the offset of the board when drawing.
        /// </summary>
        /// <value>The offset of the board.</value>
        public Vector2 Offset { get; set; }

        /// <summary>
        /// Gets the grid of tiles on the board.
        /// </summary>
        /// <value>The tile grid on the board.</value>
        public TileType[,] Grid { get; private set; }

        /// <summary>
        /// Checks if all ships on a board have been sunk.
        /// </summary>
        /// <returns>True if all ships are sunk.</returns>
        public bool AllShipsSunk()
        {
            foreach (Ship ship in this.Ships)
            {
                if (ship.Afloat)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Fires a shot at target coordinate. Returns true if a ship was hit.
        /// </summary>
        /// <param name="target">Target coordinates.</param>
        /// <returns>Boolean indicating whether a ship was hit.</returns>
        public bool FireShot(Vector2 target)
        {
            this.Grid[target.X, target.Y] = TileType.Hit;

            if (this.IsShipAtCoordinate(target))
            {
                this.GetShipAtCoordinate(target).TakeDamage(1);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if a ship is at the given coordinates.
        /// </summary>
        /// <param name="coord">The coordinates to check.</param>
        /// <returns>True of a ship is at the given coordinates.</returns>
        public bool IsShipAtCoordinate(Vector2 coord)
        {
            foreach (Ship ship in this.Ships)
            {
                if (ship.IsAtCoordinate(coord))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Tries to find a ship at the given coordinates.
        /// </summary>
        /// <param name="coord">The coordinates to check.</param>
        /// <returns>A Ship object if found, null otherwise.</returns>
        private Ship GetShipAtCoordinate(Vector2 coord)
        {
            foreach (Ship ship in this.Ships)
            {
                if (ship.IsAtCoordinate(coord))
                {
                    return ship;
                }
            }

            return null;
        }
    }
}