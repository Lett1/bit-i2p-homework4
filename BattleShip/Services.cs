﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Services.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Services type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using BattleShip.Interfaces;

    /// <summary>
    /// Provides global services for the rest of the game.
    /// </summary>
    public class Services
    {
        /// <summary>
        /// Initializes static members of the <see cref="Services"/> class.
        /// </summary>
        static Services()
        {
            Graphics = new ConsoleRenderer();
            Input = new KeyboardInput();
        }

        /// <summary>
        /// Gets the current graphics adapter.
        /// </summary>
        /// <value>The current graphics adapter.</value>
        public static IRenderer Graphics { get; }

        /// <summary>
        /// Gets the current input manager.
        /// </summary>
        /// <value>The current input manager.</value>
        public static IInputManager Input { get; }
    }
}