﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StupidAI.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the StupidAI type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    /// <summary>
    /// Simple AI that selects a random point on the map that has not been shot at yet and shoots at it.
    /// </summary>
    public class StupidAI : IComputerPlayer
    {
        /// <summary>
        /// Performs a turn of the game. If a hit is scored, another may be fired.
        /// </summary>
        /// <param name="playerBoard">The players board.</param>
        /// <param name="computerBoard">The computers board.</param>
        public void DoTurn(Board playerBoard, Board computerBoard)
        {
            bool turnOver = false;

            while (!turnOver)
            {
                if (playerBoard.AllShipsSunk())
                {
                    return;
                }

                Vector2 target = new Vector2(
                    Utilities.Random.Next(0, playerBoard.Grid.GetLength(0)),
                    Utilities.Random.Next(0, playerBoard.Grid.GetLength(1)));

                if (playerBoard.Grid[target.X, target.Y] == TileType.Hit)
                {
                    continue;
                }

                if (!playerBoard.FireShot(target))
                {
                    turnOver = true;
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.StupidAI"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.StupidAI"/>.</returns>
        public override string ToString()
        {
            return "Dumb";
        }
    }
}