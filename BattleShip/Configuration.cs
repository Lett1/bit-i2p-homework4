﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Configuration.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Holds all the configuration options for a game of battleship.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using System;

    /// <summary>
    /// Holds all the configuration options for a game of battleship.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Names of the difficulty levels.
        /// </summary>
        private static string[] difficultyLevels = { "Dumb", "Smart", "Cheating" };
        
        /// <summary>
        /// The instance of the Configuration singleton.
        /// </summary>
        private static Configuration instance;

        /// <summary>
        /// Prevents a default instance of the <see cref="Configuration"/> class from being created.
        /// </summary>
        private Configuration()
        {
            this.MoveLeftKey = ConsoleKey.LeftArrow;
            this.MoveRightKey = ConsoleKey.RightArrow;
            this.MoveUpKey = ConsoleKey.UpArrow;
            this.MoveDownKey = ConsoleKey.DownArrow;
            this.FireKey = ConsoleKey.Enter;
            this.RotateShipKey = ConsoleKey.R;

            this.BoardHeight = 10;
            this.BoardWidth = 10;

            this.SubmarineSize = 2;
            this.SubmarineCount = 5;

            this.DestroyerSize = 3;
            this.DestroyerCount = 3;

            this.CruiserSize = 4;
            this.CruiserCount = 3;

            this.BattleshipSize = 5;
            this.BattleshipCount = 1;
        }

        /// <summary>
        /// Gets the instance of the configuration singleton.
        /// </summary>
        /// <value>The configuration singleton.</value>
        public static Configuration Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Configuration();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets or sets the battleship count.
        /// </summary>
        /// <value>The battleship count.</value>
        public int BattleshipCount { get; set; }

        /// <summary>
        /// Gets or sets the battleship size.
        /// </summary>
        /// <value>The battleship size.</value>
        public int BattleshipSize { get; set; }

        /// <summary>
        /// Gets or sets the board height.
        /// </summary>
        /// <value>The board height.</value>
        public int BoardHeight { get; set; }

        /// <summary>
        /// Gets or sets the board width.
        /// </summary>
        /// <value>The board width.</value>
        public int BoardWidth { get; set; }

        /// <summary>
        /// Gets or sets the cruiser count.
        /// </summary>
        /// <value>The cruiser count.</value>
        public int CruiserCount { get; set; }

        /// <summary>
        /// Gets or sets the cruiser size.
        /// </summary>
        /// <value>The cruiser size.</value>
        public int CruiserSize { get; set; }

        /// <summary>
        /// Gets or sets the destroyer count.
        /// </summary>
        /// <value>The destroyer count.</value>
        public int DestroyerCount { get; set; }

        /// <summary>
        /// Gets or sets the destroyer size.
        /// </summary>
        /// <value>The destroyer size.</value>
        public int DestroyerSize { get; set; }

        /// <summary>
        /// Gets or sets the fire key.
        /// </summary>
        /// <value>The fire key.</value>
        public ConsoleKey FireKey { get; set; }

        /// <summary>
        /// Gets or sets the move down key.
        /// </summary>
        /// <value>The move down key.</value>
        public ConsoleKey MoveDownKey { get; set; }

        /// <summary>
        /// Gets or sets the move left key.
        /// </summary>
        /// <value>The move left key.</value>
        public ConsoleKey MoveLeftKey { get; set; }

        /// <summary>
        /// Gets or sets the move right key.
        /// </summary>
        /// <value>The move right key.</value>
        public ConsoleKey MoveRightKey { get; set; }

        /// <summary>
        /// Gets or sets the move up key.
        /// </summary>
        /// <value>The move up key.</value>
        public ConsoleKey MoveUpKey { get; set; }

        /// <summary>
        /// Gets or sets the rotate ship key.
        /// </summary>
        /// <value>The rotate ship key.</value>
        public ConsoleKey RotateShipKey { get; set; }

        /// <summary>
        /// Gets or sets the submarine count.
        /// </summary>
        /// <value>The submarine count.</value>
        public int SubmarineCount { get; set; }

        /// <summary>
        /// Gets or sets the submarine size.
        /// </summary>
        /// <value>The submarine size.</value>
        public int SubmarineSize { get; set; }

        /// <summary>
        /// Gets or sets the ai difficulty.
        /// </summary>
        /// <value>The difficulty of the AI.</value>
        public IComputerPlayer Ai { get; set; }

        /// <summary>
        /// Opens a screen that allows the user to change the settings.
        /// </summary>
        public static void ChangeSettings()
        {
            while (true)
            {
                string[] gameSettings =
                {
                    "Keyboard:",
                    $"├─Move Cursor Up:\t{Instance.MoveUpKey}",
                    $"├─Move Cursor Down:\t{Instance.MoveDownKey}",
                    $"├─Move Cursor Left:\t{Instance.MoveLeftKey}",
                    $"├─Move Cursor Right:\t{Instance.MoveRightKey}",
                    $"├─Fire At Cursor:\t{Instance.FireKey}",
                    $"└─Rotate Ship:\t{Instance.RotateShipKey}",
                    "Board:",
                    $"├─Height:\t{Instance.BoardHeight}",
                    $"└─Width:\t{Instance.BoardWidth}",
                    "Ships:",
                    $"├─Submarine Size:\t{Instance.SubmarineSize}",
                    $"├-Submarine Count:\t{Instance.SubmarineCount}",
                    $"├─Destroyer Size:\t{Instance.DestroyerSize}",
                    $"├─Destroyer Count:\t{Instance.DestroyerCount}",
                    $"├─Cruiser Size:\t{Instance.CruiserSize}",
                    $"├─Cruiser Count:\t{Instance.CruiserCount}",
                    $"├─Submarine Size:\t{Instance.CruiserSize}",
                    $"└─Submarine Count:\t{Instance.CruiserCount}",
                    $"AI Level:\t{Instance.Ai}", 
                    "Save and Exit"
                };

                int chosen = UI.ShowMenu("Change Settings", gameSettings);

                switch (chosen)
                {
                    case 1:
                        Instance.MoveUpKey = Utilities.AskForConsoleKey("Move cursor up", Instance.MoveUpKey);
                        break;
                    case 2:
                        Instance.MoveDownKey = Utilities.AskForConsoleKey("Move cursor down", Instance.MoveDownKey);
                        break;
                    case 3:
                        Instance.MoveLeftKey = Utilities.AskForConsoleKey("Move cursor left", Instance.MoveLeftKey);
                        break;
                    case 4:
                        Instance.MoveRightKey = Utilities.AskForConsoleKey("Move cursor right", Instance.MoveRightKey);
                        break;
                    case 5:
                        Instance.FireKey = Utilities.AskForConsoleKey("Fire at cursor", Instance.FireKey);
                        break;
                    case 6:
                        Instance.RotateShipKey = Utilities.AskForConsoleKey("Rotate ship", Instance.RotateShipKey);
                        break;
                    case 8:
                        Instance.BoardHeight = Utilities.AskForNumber("the board height", 10, 15);
                        break;
                    case 9:
                        Instance.BoardWidth = Utilities.AskForNumber("the board width", 10, 15);
                        break;
                    case 11:
                        Instance.SubmarineSize = Utilities.AskForNumber("the submarine size", 1, 6);
                        break;
                    case 12:
                        Instance.SubmarineCount = Utilities.AskForNumber("the amount of submarines", 1, 10);
                        break;
                    case 13:
                        Instance.DestroyerSize = Utilities.AskForNumber("the destroyer size", 1, 6);
                        break;
                    case 14:
                        Instance.DestroyerCount = Utilities.AskForNumber("the amount of destroyers", 1, 10);
                        break;
                    case 15:
                        Instance.CruiserSize = Utilities.AskForNumber("the cruiser size", 1, 6);
                        break;
                    case 16:
                        Instance.CruiserCount = Utilities.AskForNumber("the amount of cruisers", 1, 10);
                        break;
                    case 17:
                        Instance.BattleshipSize = Utilities.AskForNumber("the battleship size", 1, 6);
                        break;
                    case 18:
                        Instance.BattleshipCount = Utilities.AskForNumber("the amount of battleships", 1, 10);
                        break;
                    case 19:
                        int choice = UI.ShowMenu("How hard should the opponent be?", difficultyLevels);
                        if (choice == 0)
                        {
                            Instance.Ai = new StupidAI();
                        }
                        else if (choice == 1)
                        {
                            Instance.Ai = new SmartAI();
                        }
                        else if (choice == 2)
                        {
                            Instance.Ai = new CheatingAI();
                        }

                        break;
                    case -1:
                    case 20:
                        if (Utilities.AskForBoolean("Save and Exit?"))
                        {
                            return;
                        }

                        break;
                }
            }
        }      
    }
}