﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   The main programm class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    /// <summary>
    /// The main programm class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            string[] mainMenuItems = { "Show Help", "Change Settings", "Start Game", "Exit Game" };

            string[] victoryScreen =
            {
                "Congratulations! You sunk all enemy ships!",
                "This battle has been won but the war is not over yet.",
                "Press any key to continue."
            };
            
            string[] defeatScreen =
            {
                "All your ships have been sunk.",
                "The enemy is victorious. Don't lose hope and try again!",
                "Press any key to continue."
            };          
           
            while (true)
            {
                int chosen = UI.ShowMenu("Main Menu", mainMenuItems);

                switch (chosen)
                {
                    case 0: // Show help
                        string[] helpScreen =
                        {
                            "Welcome to Battleship! Here's how to play:",
                            " First, both players place their ships on the board.",
                            " Then they both take turns and fire at each others, ",
                            " boards,trying to sink their opponents ships.",
                            " If a hit is made, the player can shoot again.",
                            " The game ends when one player looses all of their ships.",
                            " Good luck!",
                            "Controls:",
                            $" Move Cursor Up: {Configuration.Instance.MoveUpKey}, Move Cursor Down: {Configuration.Instance.MoveDownKey}",
                            $" Move Cursor Left: {Configuration.Instance.MoveLeftKey}, Move Cursor Right: {Configuration.Instance.MoveRightKey}",
                            $" Fire at cursor: {Configuration.Instance.FireKey}, Rotate Ship (when placing): {Configuration.Instance.RotateShipKey}"
                        };
                        UI.ShowScreen(helpScreen);
                        break;
                    case 1:
                        Configuration.ChangeSettings();
                        break;
                    case 2: // Start game
                        Game game = new Game();
                        
                        int result = game.StartGame();
                        
                        if (result == 1)
                        {
                            UI.ShowScreen(victoryScreen);
                        }
                        else if (result == 0)
                        {
                            UI.ShowScreen(defeatScreen);
                        }

                        break;
                    case 3: // Quit
                        if (Utilities.AskForBoolean("Do you really want to quit?"))
                        {
                            return;
                        }

                        break;
                }
            }
        }
    }
}