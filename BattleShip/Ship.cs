﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Ship.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the Ship type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    using System;

    /// <summary>
    /// A ship that gets placed on the board.
    /// </summary>
    public class Ship
    {
        /// <summary>
        /// How much damage the ship has taken. If this is greater or equal to the size, the ship sinks.
        /// </summary>
        private int damage;

        /// <summary>
        /// Initializes a new instance of the <see cref="Ship"/> class.
        /// </summary>
        /// <param name="size">
        /// The size of the ship.
        /// </param>
        /// <param name="type">
        /// The type of the ship.
        /// </param>
        /// <param name="name">
        /// The name of the ship.
        /// </param>
        private Ship(int size, ShipType type, string name)
        {
            this.Size = size;
            this.Direction = Direction.Right;
            this.Position = new Vector2(0, 0);
            this.Afloat = true;
            this.damage = 0;
            this.Type = type;
            this.HullNumber = name;
        }

        /// <summary>
        /// Gets a value indicating whether the ship is afloat.
        /// </summary>
        /// <value>Boolean indicating whether the ship is afloat.</value>
        public bool Afloat { get; private set; }

        /// <summary>
        /// Gets or sets the direction that the ship is facing.
        /// </summary>
        /// <value>Direction the ship is facing.</value>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets the hull number of the ship.
        /// </summary>
        /// <value>The name of the ship.</value>
        public string HullNumber { get; private set; }

        /// <summary>
        /// Gets or sets the ships position on the board.
        /// </summary>
        /// <value>The position of the ship.</value>
        public Vector2 Position { get; set; }

        /// <summary>
        /// Gets the size of the ship.
        /// </summary>
        /// <value>The size of the ship.</value>
        public int Size { get; private set; }

        /// <summary>
        /// Gets the type of the ship.
        /// </summary>
        /// <value>The type of the ship.</value>
        public ShipType Type { get; private set; }

        /// <summary>
        /// Create and returns a new ship instance with the corresponding size and name.
        /// </summary>
        /// <param name="type">What type of ship to create.</param>
        /// <returns>The new ship.</returns>
        public static Ship AssembleShip(ShipType type)
        {
            int size = 0;
            string name = string.Empty;
            switch (type)
            {
                case ShipType.Destroyer:
                    size = Configuration.Instance.DestroyerSize;
                    name = $"DD-{Utilities.Random.Next(1, 200)}";
                    break;
                case ShipType.Submarine:
                    size = Configuration.Instance.SubmarineSize;
                    name = $"SS-{Utilities.Random.Next(1, 200)}";
                    break;
                case ShipType.Cruiser:
                    size = Configuration.Instance.CruiserSize;
                    name = $"CL-{Utilities.Random.Next(1, 200)}";
                    break;
                case ShipType.Battleship:
                    size = Configuration.Instance.BattleshipSize;
                    name = $"BB-{Utilities.Random.Next(1, 50)}";
                    break;
            }

            return new Ship(size, type, name);
        }

        /// <summary>
        /// Gets the points on the grid that are occupied by this ship.
        /// </summary>
        /// <returns>Vector2[] of points.</returns>
        public Vector2[] GetOccupiedPoints()
        {
            Vector2 temp = this.Position;
            Vector2 directionVector = this.GetDirectionVector();
            Vector2[] points = new Vector2[this.Size];

            for (int i = 0; i < this.Size; i++)
            {
                points[i] = temp;
                temp = temp.Add(directionVector);
            }

            return points;
        }

        /// <summary>
        /// Checks if the ship is at the given coordinate.
        /// </summary>
        /// <param name="coord">Board coordinate to check.</param>
        /// <returns>True of the ship is at the coordinate.</returns>
        public bool IsAtCoordinate(Vector2 coord)
        {
            Vector2 directionVector = this.GetDirectionVector();
            Vector2 temp = this.Position;

            for (int i = 0; i < this.Size; i++)
            {
                if (temp.Equals(coord))
                {
                    return true;
                }

                temp = temp.Add(directionVector);
            }

            return false;
        }

        /// <summary>
        /// Deals a specified amount of damage to the ship and sinks it if it exceeds its size.
        /// </summary>
        /// <param name="amount">The amount of damage to deal.</param>
        public void TakeDamage(int amount)
        {
            this.damage += amount;

            if (this.damage >= this.Size)
            {
                this.Afloat = false;
            }
        }

        /// <summary>
        /// Returns a string representation of the ship in the format: "{this.type} @ {this.position}".
        /// </summary>
        /// <returns>The string version of the ship.</returns>
        public override string ToString()
        {
            return $"{this.Type} @ {this.Position}";
        }

        /// <summary>
        /// Gets a Vector that represents the movement in the specified direction.
        /// </summary>
        /// <returns>A Vector2.</returns>
        private Vector2 GetDirectionVector()
        {
            switch (this.Direction)
            {
                case Direction.Up:
                    return new Vector2(0, -1);
                case Direction.Down:
                    return new Vector2(0, 1);
                case Direction.Left:
                    return new Vector2(-1, 0);
                case Direction.Right:
                    return new Vector2(1, 0);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}