﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IComputerPlayer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the IComputerPlayer.cs type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    /// <summary>
    /// The ComputerPlayer interface.
    /// </summary>
    public interface IComputerPlayer
    {
        /// <summary>
        /// Does a turn in the game.
        /// </summary>
        /// <param name="playerBoard">
        /// The player board.
        /// </param>
        /// <param name="computerBoard">
        /// The computer board.
        /// </param>
        void DoTurn(Board playerBoard, Board computerBoard);
    }
}