﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRenderer.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the IRenderer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip.Interfaces
{
    using System;

    /// <summary>
    /// The Renderer interface.
    /// </summary>
    public interface IRenderer
    {
        /// <summary>
        /// Clear the screen.
        /// </summary>
        void Clear();

        /// <summary>
        /// Get the background color.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleColor"/>.
        /// </returns>
        ConsoleColor GetBackgroundColor();

        /// <summary>
        /// Get the foreground color.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleColor"/>.
        /// </returns>
        ConsoleColor GetForegroundColor();

        /// <summary>
        /// Get the height of the viewport.
        /// </summary>
        /// <returns>
        /// The height of the viewport.
        /// </returns>
        int GetViewportHeight();

        /// <summary>
        /// Get the width of the viewport.
        /// </summary>
        /// <returns>
        /// The width of the viewport.
        /// </returns>
        int GetViewportWidth();

        /// <summary>
        /// Get the bounds of the window.
        /// </summary>
        /// <returns>
        /// The <see cref="Vector2"/>.
        /// </returns>
        Vector2 GetWindowBounds();

        /// <summary>
        /// Draw a single char at the specified coordinates.
        /// </summary>
        /// <param name="x">
        /// The x position.
        /// </param>
        /// <param name="y">
        /// The y position.
        /// </param>
        /// <param name="c">
        /// The character to draw.
        /// </param>
        void PutChar(int x, int y, char c);

        /// <summary>
        /// Draw a string at the specified position.
        /// </summary>
        /// <param name="x">
        /// The x position.
        /// </param>
        /// <param name="y">
        /// The y position.
        /// </param>
        /// <param name="text">
        /// The text to draw.
        /// </param>
        void PutString(int x, int y, string text);

        /// <summary>
        /// Reset colors to the console defaults.
        /// </summary>
        void ResetColors();

        /// <summary>
        /// Set the background color.
        /// </summary>
        /// <param name="background">
        /// The background.
        /// </param>
        void SetBackgroundColor(ConsoleColor background);

        /// <summary>
        /// Set the foreground color.
        /// </summary>
        /// <param name="foreground">
        /// The foreground.
        /// </param>
        void SetForegroundColor(ConsoleColor foreground);

        /// <summary>
        /// Perform setup of the Renderer.
        /// </summary>
        void Setup();
    }
}