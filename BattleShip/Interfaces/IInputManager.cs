﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInputManager.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the IInputManager type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip.Interfaces
{
    using System;

    /// <summary>
    /// The InputManager interface.
    /// </summary>
    public interface IInputManager
    {
        /// <summary>
        /// Whether a key has been pressed but not processed yet.
        /// </summary>
        /// <returns>
        /// True if keys have been pressed but not processed yet.
        /// </returns>
        bool KeyAvailable();

        /// <summary>
        /// Read a single Key from input.
        /// </summary>
        /// <returns>
        /// The <see cref="ConsoleKey"/>.
        /// </returns>
        ConsoleKey ReadKey();

        /// <summary>
        /// Read a string from standard input.
        /// </summary>
        /// <returns>The entered string.</returns>
        string ReadLine();

        /// <summary>
        /// Wait for a key press of one or more specific keys. Or just any key.
        /// </summary>
        /// <param name="keys">
        /// The keys to wait for.
        /// </param>
        void WaitForKeyPress(params ConsoleKey[] keys);
    }
}