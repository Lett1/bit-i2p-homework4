﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CheatingAI.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the CheatingAI type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace BattleShip
{
    using System.Collections.Generic;

    /// <summary>
    /// A cheating ai for a game of battleship. Knows where the players ships are and has a small chance to directly target them.
    /// </summary>
    public class CheatingAI : IComputerPlayer
    {
        /// <summary>
        /// The ship positions.
        /// </summary>
        private List<Vector2> shipPositions;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:BattleShip.CheatingAI"/> class.
        /// </summary>
        public CheatingAI()
        {
            this.shipPositions = new List<Vector2>();
        }
        
        /// <summary>
        /// Performs a turn of the game. If a hit is scored, another may be fired.
        /// </summary>
        /// <param name="playerBoard">The players board.</param>
        /// <param name="computerBoard">The computers board.</param>
        public void DoTurn(Board playerBoard, Board computerBoard)
        {
            bool turnOver = false;

            while (!turnOver)
            {
                if (playerBoard.AllShipsSunk())
                {
                    return;
                }

                // Get all ship positions that haven't been shot at yet.
                foreach (Ship ship in playerBoard.Ships)
                {
                    foreach (Vector2 point in ship.GetOccupiedPoints())
                    {
                        if (playerBoard.Grid[point.X, point.Y] != TileType.Hit)
                        {
                            this.shipPositions.Add(point);
                        }
                    }
                }
                
                Vector2 target;

                if (Utilities.Random.Next(0, 100) > 30)
                {
                    int index = Utilities.Random.Next(0, this.shipPositions.Count);
                    target = this.shipPositions[index];
                    this.shipPositions.RemoveAt(index);
                }
                else   
                {
                   target = new Vector2(
                        Utilities.Random.Next(0, playerBoard.Grid.GetLength(0)),
                        Utilities.Random.Next(0, playerBoard.Grid.GetLength(1)));
                }

                if (playerBoard.Grid[target.X, target.Y] == TileType.Hit)
                {
                    continue;
                }

                if (!playerBoard.FireShot(target))
                {
                    turnOver = true;
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.CheatingAI"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:BattleShip.CheatingAI"/>.</returns>
        public override string ToString()
        {
            return "Cheating";
        }
    }
}