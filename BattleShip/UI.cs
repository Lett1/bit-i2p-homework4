﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UI.cs" company="LettSoft">
//   Copyright LettSoft 2017
// </copyright>
// <summary>
//   Defines the UI type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace BattleShip
{
    using System;
    
    /// <summary>
    /// Provides various functions to draw UI elements.
    /// </summary>
    public static class UI
    {
        /// <summary>
        /// Show help text and wait for any key press.
        /// </summary>
        /// <param name="text">
        /// Array of strings to display.
        /// </param>
        public static void ShowScreen(string[] text)
        {
            Services.Graphics.Clear();

            for (int line = 0; line < text.Length; line++)
            {
                Services.Graphics.PutString(0, line, text[line]);
            }

            Services.Input.WaitForKeyPress();
        }

        /// <summary>
        /// Shows a menu and allows the user to choose an entry.
        /// </summary>
        /// <returns>Integer representing the chosen option.</returns>
        /// <param name="title">Menu title.</param>
        /// <param name="items">Menu items.</param>
        /// <param name="descriptions">Item descriptions.</param>
        public static int ShowMenu(string title, string[] items, string[] descriptions = null)
        {
            int index = 0;

            while (true)
            {
                Services.Graphics.SetBackgroundColor(ConsoleColor.Black); // Make the screen clear to black
                Services.Graphics.Clear();

                DrawTextBar(title, backgroundColor: ConsoleColor.Blue);

                for (int i = 0; i < items.Length; i++)
                {
                    if (i == index)
                    {
                        Services.Graphics.SetBackgroundColor(ConsoleColor.White);
                        Services.Graphics.SetForegroundColor(ConsoleColor.Black);
                    }

                    Services.Graphics.PutString(0, i + 1, items[i]);
                    if (descriptions != null)
                    {
                        DrawTextBar(descriptions[index], y: Services.Graphics.GetViewportHeight() - 1);
                    }

                    Services.Graphics.SetBackgroundColor(ConsoleColor.Black);
                    Services.Graphics.SetForegroundColor(ConsoleColor.White);
                }

                ConsoleKey input = Services.Input.ReadKey();

                switch (input)
                {
                    case ConsoleKey.DownArrow:
                        if (index < items.Length - 1)
                        {
                            index++;
                        }

                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                        {
                            index--;
                        }

                        break;
                    case ConsoleKey.Enter:
                        return index;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }

        /// <summary>
        /// Draws a colored bar of text.
        /// </summary>
        /// <param name="title">Content of the bar.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="foregroundColor">Foreground color.</param>
        /// <param name="backgroundColor">Background color.</param>
        private static void DrawTextBar(
            string title,
            int x = 0,
            int y = 0,
            ConsoleColor foregroundColor = ConsoleColor.Black,
            ConsoleColor backgroundColor = ConsoleColor.White)
        {
            ConsoleColor oldForeground = Services.Graphics.GetForegroundColor();
            ConsoleColor oldBackground = Services.Graphics.GetBackgroundColor();

            Services.Graphics.SetBackgroundColor(backgroundColor);
            Services.Graphics.SetForegroundColor(foregroundColor);

            Services.Graphics.PutString(x, y, title + "\n");

            Services.Graphics.SetBackgroundColor(oldBackground);
            Services.Graphics.SetForegroundColor(oldForeground);
        }
    }
}